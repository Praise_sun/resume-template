module.exports = {
	list: [{
		"name": "伊了健康",
		"time": "2022.11-至今",
		"tags": "vue,h5,uniapp,小程序",
		"duty": "1. 负责前端系统项目框架搭建，负责后台系统、h5、小程序等项目的落地实施；\n2. 解决项目多端兼容、优化性能体验；\n3. 负责视频管理、即时通讯、线上问诊、商城模块等模块开发；\n4. 参与产品项目研发，完成项目版本迭代。",
		"covers": [{
				"url": "https://mp-3ad318f6-bda9-4e5c-9962-7e028b4b8932.cdn.bspapp.com/cloudstorage/489936c7-945a-4ae1-a9ec-a749403ec24e.png",
			},
			{
				"url": "https://mp-3ad318f6-bda9-4e5c-9962-7e028b4b8932.cdn.bspapp.com/cloudstorage/6e9099e5-ca22-49f7-bbf7-c33d2dc2643c.png",
			},
			{
				"url": "https://mp-3ad318f6-bda9-4e5c-9962-7e028b4b8932.cdn.bspapp.com/cloudstorage/369ce757-65f1-41d9-92ba-2e8f767ca280.png",
			},
			{
				"url": "https://mp-3ad318f6-bda9-4e5c-9962-7e028b4b8932.cdn.bspapp.com/cloudstorage/465fecdf-45e2-4f90-825b-a49a13e34398.png",
			},
			{
				"url": "https://mp-3ad318f6-bda9-4e5c-9962-7e028b4b8932.cdn.bspapp.com/cloudstorage/069cf05f-80a2-4d67-b629-2ee652cb4f82.png",
			},
			{
				"url": "https://mp-3ad318f6-bda9-4e5c-9962-7e028b4b8932.cdn.bspapp.com/cloudstorage/bdff8b7f-8ef1-4666-9367-2c027c5b38a6.png",
			}
		],
	}, {
		"name": "立达信近视防控平台",
		"time": "2022.07-2022.08",
		"tags": "vue3,ts,antdesign,uniapp,小程序",
		"duty": "1. 负责后台可视化图表生成、问卷模块功能开发；\n2. 使用uniapp完成小程序、h5页面项目开发，完成多端兼容；\n3. 负责检查工作、问卷调查、视力分析等模块开发；\n4. 使用蓝牙模块相关api对视力设备进行联调，传输保存相关检查数据。",
		"covers": [],
	}, {
		"name": "新津资产管理系统",
		"time": "2022.05-2022.06",
		"tags": "vue,element,echarts",
		"duty": "1. 负责平台系统前端框架、页面布局搭建；\n2. 负责菜单管理、公共组件、请求拦截器封装等前端开发工作；\n3. 完成地图点位绘制、可视化图表生成等功能的开发。",
		"covers": [],
	}, {
		"name": "天天拿",
		"time": "2022.02-2022.03",
		"tags": "vue,uniapp,小程序",
		"duty": "1. 负责小程序资讯、商城、夺宝、接龙等模块前端开发工作；\n2. 对接微信支付完成相关交易功能；\n3. 完成转盘、接龙、创豆、拼团、邀请分销等营销活动功能的开发；\n4. 负责埋点需求，记录用户页面浏览操作功能开发。",
		"covers": [{
				"url": "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3ad318f6-bda9-4e5c-9962-7e028b4b8932/8c85f76f-3fef-476f-9fb1-adac39ea5b4a.png",
			},
			{
				"url": "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3ad318f6-bda9-4e5c-9962-7e028b4b8932/737f762c-25e0-495e-a22f-be26b81f18aa.png",
			},
			{
				"url": "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3ad318f6-bda9-4e5c-9962-7e028b4b8932/20f162ae-3598-4d78-843c-aaa38d35fce0.png",
			}
		]
	}, {
		"name": "吉比特·在线学习平台",
		"time": "2020.08-2020.10",
		"tags": "vue,element,uniapp",
		"duty": "1. 负责后台管理系统课程、培训、问卷、考试、知识库等模块的开发；\n2. 移动端使用uniapp框架进行知识库、问卷及考试模块开发；\n3. 实现产品所需的交互效果，解决适配及 iOS 兼容问题。",
		"covers": [],
	}]
}