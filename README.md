### 说明
> 展示个人简历的页面模板

### 页面展示
![pit3PPA.jpg](https://z1.ax1x.com/2023/11/17/pitWfHO.jpg)

### 预览地址
##### [网站：前端路上的旅行](https://www.zouwq.com)
![pitUt6s.jpg](https://z1.ax1x.com/2023/11/17/pitW5Ue.jpg)